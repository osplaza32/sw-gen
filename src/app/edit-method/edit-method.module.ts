import { NgModule, Injectable } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { EditMethodPage } from './edit-method.page';
import { MdModule } from '../md/md.module';
import {HttpClientModule} from '@angular/common/http';

const routes: Routes = [
  {
    path: '',
    component: EditMethodPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    MdModule,
    HttpClientModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EditMethodPage]
})
export class EditMethodPageModule {}
